package com.socialgroup.chat.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.socialgroup.chat.configuration.converter.ZonedDateTimeReadConverter;
import com.socialgroup.chat.configuration.converter.ZonedDateTimeWriteConverter;

@Configuration
public class MongoDBConfiguration extends AbstractMongoClientConfiguration {

	private final List<Converter<?, ?>> converters = new ArrayList<Converter<?, ?>>();

	@Value("${mongodb.auth.username}")
	private String username;

	@Value("${mongodb.auth.password}")
	private String password;

	@Value("${mongodb.auth.database}")
	private String database;

	@Value("${mongodb.auth.host}")
	private String host;

	@Value("${mongodb.auth.port}")
	private String port;

	@Override
	public MongoClient mongoClient() {
		return MongoClients.create(String.format("mongodb://%s:%s@%s:%s/?authSource=%s", username, password, host, port, database));
	}

	@Override
	protected String getDatabaseName() {
		return "socialgroup";
	}

	@Override
    public MongoCustomConversions customConversions() {
        converters.add(new ZonedDateTimeReadConverter());
        converters.add(new ZonedDateTimeWriteConverter());
        return new MongoCustomConversions(converters);
    }

	@Bean
    MongoTransactionManager transactionManager(final MongoDbFactory dbFactory) {
		return new MongoTransactionManager(dbFactory);
    }

	@Override
	public @Bean MongoTemplate mongoTemplate() {
		return new MongoTemplate(mongoClient(), getDatabaseName());
	}

	@Override
	public boolean autoIndexCreation() {
		return true;
	}

}
