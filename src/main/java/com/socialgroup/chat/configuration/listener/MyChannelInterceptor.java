package com.socialgroup.chat.configuration.listener;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.web.client.RestTemplate;

import com.socialgroup.chat.service.WsManager;

public class MyChannelInterceptor implements ChannelInterceptor {

	private final WsManager wsManager;

	public MyChannelInterceptor(final String authApiPath, final RestTemplate restTemplate) {
		this.wsManager = new WsManager(authApiPath, restTemplate);
	}

	@Override
	public Message<?> preSend(final Message<?> message, final MessageChannel channel) {

		final StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

		if (StompCommand.CONNECT.equals(accessor.getCommand())) {

			if(this.haveAccess(message, accessor) == null || !wsManager.containsKeyNativeHeaders(accessor) || accessor.getFirstNativeHeader("X-Authorization") == null) {
				return null;
			}

			final Long user = wsManager.userConnected(accessor.getFirstNativeHeader("X-Authorization"));
			if(user == null) {
				return null;
			}

			wsManager.removeKey(user);
		}

		if (StompCommand.SUBSCRIBE.equals(accessor.getCommand())) {

			if(this.haveAccess(message, accessor) == null || !wsManager.containsKeyNativeHeaders(accessor)
					|| accessor.getFirstNativeHeader("X-Authorization") == null || accessor.getFirstNativeHeader("destination") == null) {
				return null;
			}

			final Long user = wsManager.userConnected(accessor.getFirstNativeHeader("X-Authorization"));
			if(user == null) {
				return null;
			}

			if(!this.wsManager.pathExist(accessor.getFirstNativeHeader("destination"))) {
				return null;
			}

			if(!wsManager.containRoom(user, accessor.getFirstNativeHeader("destination"))) {
				if (!wsManager.addRoom(user, accessor.getFirstNativeHeader("destination"), accessor.getFirstNativeHeader("zone"))) {
					return null;
				}
			}
			else {
				return null;
			}
		}

		if (StompCommand.SEND.equals(accessor.getCommand())) {

			if(this.haveAccess(message, accessor) == null || !wsManager.containsKeyNativeHeaders(accessor)
					|| accessor.getFirstNativeHeader("X-Authorization") == null || accessor.getFirstNativeHeader("destination") == null) {
				return null;
			}

			final Long user = wsManager.userConnected(accessor.getFirstNativeHeader("X-Authorization"));

			if(user == null) {
				return null;
			}

		}

		return message;
	}

	/**
	 * Check minimal condition to access
	 * @param message
	 * @param accessor
	 * @return
	 */
	private Message<?> haveAccess(final Message<?> message, final StompHeaderAccessor accessor) {

		if(!accessor.containsNativeHeader("X-Authorization")) {
			return null;
		}

		final String token = accessor.getFirstNativeHeader("X-Authorization");
		if ((token == null || token.isEmpty())) {
			return null;
		}

		if(!this.wsManager.userExist(token)) {
			return null;
		}

		return message;
	}

//	@Override
//	public void afterReceiveCompletion(@Nullable Message<?> message, MessageChannel channel, @Nullable Exception ex) {
//		System.out.println("afterReceiveCompletion");
//	}
//
//	@Override
//	public Message<?> postReceive(Message<?> message, MessageChannel channel) {
//		System.out.println("afterReceiveCompletion");
//		return message;
//	}
//
//	@Override
//	public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
//		System.out.println("afterReceiveCompletion");
//	}
//
//	@Override
//	public void afterSendCompletion(Message<?> message, MessageChannel channel, boolean sent, @Nullable Exception ex) {
//		System.out.println("afterSendCompletion");
//	}

}
