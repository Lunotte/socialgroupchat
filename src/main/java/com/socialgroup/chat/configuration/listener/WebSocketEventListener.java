package com.socialgroup.chat.configuration.listener;

import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import com.socialgroup.chat.controller.ChatController;
import com.socialgroup.chat.service.WsManager;

@Component
public class WebSocketEventListener {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

    private final ChatController chatController;
    private final WsManager wsManager;

    public WebSocketEventListener(final ChatController chatController, final WsManager wsManager) {
        this.chatController = chatController;
        this.wsManager = wsManager;
    }

    @EventListener
    public void handleWebSocketConnectListener(final SessionConnectedEvent event) {
    	logger.info("Received a new web socket connection.");
    }

    /**
     * Nouvelle socket à laquel on souscrit
     * @param event
     */
    @EventListener
    public void handleWebSocketConnectListener(final SessionSubscribeEvent event) {
    	logger.info("Received a new web socket connection.");
    	if(wsManager.containsKeyNativeHeaders(event)) {
        	final LinkedList<String> val = wsManager.getCustomHeaderPersonalParameter(event);
        	if(val.getFirst().equals("false")) {
        		chatController.sendPersonalSubsription(wsManager.getDestination(event));
        	}
    	}
    }

}