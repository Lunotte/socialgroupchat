package com.socialgroup.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.socialgroup.chat.repository.RoomRepository;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableDiscoveryClient
@EnableSwagger2
public class SocialGroupChatApplication implements CommandLineRunner{

	@Autowired
	private RoomRepository roomRepository;

	public static void main(final String[] args) {
		SpringApplication.run(SocialGroupChatApplication.class, args);
	}

	@Override
	public void run(final String... args) throws Exception {
//		roomRepository.deleteAll();
//
//		roomRepository.save(new Room(1L, 7L, RoomTypeEnum.USER, ZoneId.of("Europe/Paris")));
//
//		for (final Room room : roomRepository.findAll()) {
//
//			System.out.println(room.toString());
//		}
	}

}
