package com.socialgroup.chat.controller;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;

import com.socialgroup.chat.service.RoomService;

@Controller
public class ChatController {

	private final SimpMessageSendingOperations messagingTemplate;
	private final RoomService roomService;

	public ChatController(final SimpMessageSendingOperations messagingTemplate, final RoomService roomService) {
        this.messagingTemplate = messagingTemplate;
        this.roomService = roomService;
    }

	@MessageMapping("/group/{groupId}")
    public void group(final String message, @DestinationVariable("groupId") final String groupId, @Header("X-Authorization") final String authorization) throws Exception {
		final String destination = "/topic/group/" + groupId;
		this.roomService.addNewMessage(authorization, destination, message);
		this.messagingTemplate.convertAndSend(destination, message);
    }

	@MessageMapping("/group/{groupId}/user/{userId}")
    public void user(final String message, @DestinationVariable("groupId") final String groupId, @DestinationVariable("userId") final String userId) throws Exception {
		this.messagingTemplate.convertAndSend("/queue/group/" + groupId + "/user/" + userId, "Une message pour un utilisateur " + message);
    }

    /***
     * Notifie l'utilisateur de sa souscription
     * @param path
     */
    public void sendPersonalSubsription(final String path) {
    	messagingTemplate.convertAndSend("/queue/personal/1", path);
    }

//    public void handleUserDisconnection(final String userName) {
//    	System.out.println("User disconnected.");
//    }

}