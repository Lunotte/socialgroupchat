package com.socialgroup.chat.model;

import org.springframework.data.mongodb.core.index.HashIndexed;
import org.springframework.data.mongodb.core.index.Indexed;

public class DomainTypeRoommField {

	@Indexed
	@HashIndexed
	final String value = "roomField";

	public String getValue()
	{
		return value;
	}

}
