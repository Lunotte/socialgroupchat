package com.socialgroup.chat.model;

public class Information {

	private final Long from;
	private final Long to;
	private final RoomTypeEnum roomType;
	private final String destination;
	private final String zone;

	public Information(final Long from, final String destination, final String zone)
	{
		this.from = from;
		this.destination = destination;
		this.roomType = whichRoomType();
		this.to = userDestination();
		this.zone = zone;
	}

	private Long userDestination() {
		Long to = null;
		switch (this.roomType) {
			case GROUP:
				to = findGroup();
				break;
			case USER:
				to = findUser();
				break;
			default:
				break;
		}
		return to;
	}

	private RoomTypeEnum whichRoomType() {

		if(this.destination.startsWith("/queue/personal/")) {
			return RoomTypeEnum.PERSONAL;
		}

		return (this.destination.contains("/user/")) ? RoomTypeEnum.USER : RoomTypeEnum.GROUP;
	}

	private Long findGroup() {
		return null;
	}

	private Long findUser() {
		 final String[] values = this.destination.split("/queue/group/[0-9]+/user/");
		 return Long.valueOf(values[1]);
	}

	public Long getFrom()
	{
		return from;
	}

	public Long getTo()
	{
		return to;
	}

	public RoomTypeEnum getRoomType()
	{
		return roomType;
	}

	public String getDestination()
	{
		return destination;
	}

	public String getZone()
	{
		return zone;
	}

}
