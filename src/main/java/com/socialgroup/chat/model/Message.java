package com.socialgroup.chat.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Message {

	@Id
	private ObjectId id;
	private String content;
	private Date date;

	public Message(final String content) {
		this.id = new ObjectId();
		this.content = content;
		this.date =  new Date();
	}

	public ObjectId getId() {
		return id;
	}
	public String getContent() {
		return content;
	}
	public Date getDate() {
		return date;
	}

}
