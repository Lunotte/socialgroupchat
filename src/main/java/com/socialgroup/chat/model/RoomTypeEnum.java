package com.socialgroup.chat.model;


public enum RoomTypeEnum {

	GROUP,
	USER,
	PERSONAL

}
