package com.socialgroup.chat.model;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Room {

	@Id
	private ObjectId id;

	private Long from;

	private Long to;

	private Collection<Message> messages = new ArrayList<>();

	@Indexed(name = "roomIndex", direction = IndexDirection.ASCENDING)
	private String room; // Combinaison des 2 personnes qui communiques pour créer un identifiant de conversation. Ordre du code {idLeplusPetit-idLeplusGrand}

	private RoomTypeEnum roomType;

	private Date date;

	private ZoneId zone;

	public Room() { }

	public Room(final Long from, final Long to, final RoomTypeEnum roomType, final ZoneId zone) {
		this.from = from;
		this.to = to;
		this.room = this.buildRoom();
		this.roomType = roomType;
		this.date = new Date();
		this.zone = zone;
		this.messages = new ArrayList<>();
	}

	public Room(final Long from, final Long to) {
		this.from = from;
		this.to = to;
		this.room = this.buildRoom();
		//this.date = ZonedDateTime.now();
	}

	public Room(final Information information)
	{
		this.from = information.getFrom();
		this.to = information.getTo();
		this.room = this.buildRoom();
		this.roomType = information.getRoomType();
		this.date = new Date();
		this.zone = ZoneId.of(information.getZone());
	}

	private String buildRoom() {
		if(this.to == null) {
			return null;
		}
		return (this.from < this.to) ? this.from + "-" + this.to : this.to + "-" + this.from;
	}

	public Long getFrom() {
		return from;
	}
	public void setFrom(final Long from) {
		this.from = from;
	}
	public Long getTo() {
		return to;
	}
	public void setTo(final Long to) {
		this.to = to;
	}
	public Collection<Message> getMessages() {
		return messages;
	}
	public void addMessage(final Message message) {
		this.messages.add(message);
	}

	public String getRoom() {
		return room;
	}
	public void setRoom(final String room) {
		this.room = room;
	}
	public Date getDate() {
		return date;
	}

	public RoomTypeEnum getRoomType()
	{
		return roomType;
	}


	public void setRoomType(final RoomTypeEnum roomType)
	{
		this.roomType = roomType;
	}


	public ZoneId getZone()
	{
		return zone;
	}

}
