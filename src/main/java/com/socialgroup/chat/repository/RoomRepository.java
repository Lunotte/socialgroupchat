package com.socialgroup.chat.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.socialgroup.chat.model.Room;
import com.socialgroup.chat.model.RoomTypeEnum;

public interface RoomRepository extends MongoRepository<Room, String>{

	@Override
	public Optional<Room> findById(String id);

	public Room findByFromAndRoomType(Long from, RoomTypeEnum roomType);

	public Room findByFromAndRoomType(Long from, Long to, RoomTypeEnum roomType);

	@Override
	public List<Room> findAll();
}
