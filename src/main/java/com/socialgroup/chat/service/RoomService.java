package com.socialgroup.chat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.socialgroup.chat.model.Information;
import com.socialgroup.chat.model.Message;
import com.socialgroup.chat.model.Room;
import com.socialgroup.chat.model.RoomTypeEnum;
import com.socialgroup.chat.repository.RoomRepository;

@Service
public class RoomService {

	private final WsManager wsManager;
	private final RoomRepository roomRepository;

	public RoomService(final RoomRepository roomRepository, final RestTemplate restTemplate, final String authApiPath)
	{
		this.roomRepository = roomRepository;
		this.wsManager = new WsManager(authApiPath, restTemplate);
	}

	public Room findRoomGroup(final Information information) throws Exception {
		try
		{
			Room room = null;

			room = this.roomRepository.findByFromAndRoomType(information.getFrom(), RoomTypeEnum.GROUP);
			if(room == null) {
				room = saveRoom(information);
			}
			return room;

		} catch (final Exception e)
		{
			throw new Exception("An error has occured", e);
		}
	}

	public Room findRoomUser(final Information information) throws Exception {
		try
		{
			return this.roomRepository.findByFromAndRoomType(information.getFrom(), information.getTo(), RoomTypeEnum.USER);

		} catch (final Exception e)
		{
			throw new Exception("An error has occured", e);
		}
	}

	public Room saveRoom(final Information information) throws Exception {

		try
		{
			return this.roomRepository.save(new Room(information));

		} catch (final Exception e)
		{
			throw new Exception("An error has occured", e);
		}
	}

	public Room saveMessage(final Information information, final String message) throws Exception {

		try
		{
			Room room = null;
			if(information.getRoomType().equals(RoomTypeEnum.GROUP)) {
				room = findRoomGroup(information);
			} else {
				room = findRoomUser(information);
			}

			final Message m = new Message(message);
			room.addMessage(m);
			return this.roomRepository.save(room);

		} catch (final Exception e)
		{
			throw new Exception("An error has occured", e);
		}
	}

	public void addNewMessage(final String token, final String destination, final String message) {
		final Long user = this.wsManager.userConnected(token);
		final Optional<Information> information = this.wsManager.getRoom(user, destination);

		if(information.isPresent()) {
			try
			{
				saveMessage(information.get(), message);
			} catch (final Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void findLastMessage(final long pageNumber, final long nbPerPage) {
	    final Room room = this.roomRepository.findByFromAndRoomType(1L, RoomTypeEnum.GROUP);

	    final List<Message> m = new ArrayList<Message>();
	   // final long pageNumber = 1;
	  //  final long nbPerPage = 5;
	    room.getMessages().stream().skip( pageNumber > 0 ? ( ( pageNumber - 1 ) * nbPerPage ) : 0 )
	             .limit( nbPerPage )
	             .forEach(d -> m.add(d));
	    System.out.println(m);
	}

}
