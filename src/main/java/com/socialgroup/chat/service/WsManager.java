package com.socialgroup.chat.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;

import com.socialgroup.chat.model.Information;

@Service
public class WsManager {

	private final String authApiPath;
	private final RestTemplate restTemplate;

	public static String pathPersonalString = "^\\/queue\\/personal\\/[0-9]+$";
	public static String pathGroupString = "^\\/topic\\/group\\/[0-9]+$";
	public static String pathUserString = "^\\/queue\\/group\\/[0-9]+\\/user\\/[0-9]+$";

	private static final Pattern pathPersonal = Pattern.compile(pathPersonalString);
	private static final Pattern pathGroup = Pattern.compile(pathGroupString);
	private static final Pattern pathUser = Pattern.compile(pathUserString);

	private static final Collection<Pattern> paths = Arrays.asList(pathPersonal, pathGroup, pathUser);
	private static Map<Long, Set<Information>> listRoom = new HashMap<>();

	public WsManager() {
		this.authApiPath = null;
		this.restTemplate = null;
	}

	public WsManager(final String authApiPath, final RestTemplate restTemplate) {
		this.authApiPath = authApiPath;
		this.restTemplate = restTemplate;
	}

	private MessageHeaders getHeaders(final SessionSubscribeEvent event) {
		return event.getMessage().getHeaders();
	}

	private Object getHeaders(final StompHeaderAccessor stompHeaderAccessor) {
		return stompHeaderAccessor.getHeader("nativeHeaders");
	}

	public LinkedList<String> getCustomHeaderPersonalParameter(final SessionSubscribeEvent event) {
		@SuppressWarnings("unchecked")
		final
		Map<String, List<String>> nativeHeaders = (Map<String, List<String>>) getHeaders(event).get("nativeHeaders");
		return (LinkedList<String>) nativeHeaders.get("personal");
	}

	public boolean containsKeyNativeHeaders(final SessionSubscribeEvent event) {
		return getHeaders(event).containsKey("nativeHeaders");
	}

	public boolean containsKeyNativeHeaders(final StompHeaderAccessor stompHeaderAccessor) {
		return getHeaders(stompHeaderAccessor) != null;
	}

	public void removeKey(final Long key) {
		if (listRoom.containsKey(key)) {
			listRoom.remove(key);
		}
	}

	public String getDestination(final SessionSubscribeEvent event) {
		return String.valueOf(getHeaders(event).get("simpDestination"));
	}

	public boolean addRoom(final Long user, final String room, final String zone) {
		if(listRoom.containsKey(user)) {
			if (!containRoom(user, room)) {
				final Set<Information> listRoomTmp = listRoom.get(user);
				listRoomTmp.add(new Information(user, room, zone));
				listRoom.put(user, listRoomTmp);
				return true;
			}
			return false;
		} else {
			listRoom.put(user, Stream.of(new Information(user, room, zone)).collect(Collectors.toSet()));
			return true;
		}
	}

	public boolean containRoom(final Long user, final String room) {
		if(listRoom.containsKey(user)) {
			return listRoom.get(user).stream().anyMatch(r -> r.getDestination().equals(room));
		}
		return false;
	}

	public Optional<Information> getRoom(final Long user, final String room) {

		if(listRoom.containsKey(user)) {
			return listRoom.get(user).stream().filter(r -> r.getDestination().endsWith(room)).findFirst();
		}
		return Optional.empty();
	}

	private HttpEntity<String> httpHeaders(final String token) {
		final HttpHeaders headers = new HttpHeaders();
		headers.set("X-Authorization", "Bearer " + token);
		return new HttpEntity<>("body", headers);
	}

	public Boolean userExist(final String token) {
		return restTemplate.exchange("http://" + authApiPath + "/user-exist",
				HttpMethod.GET, httpHeaders(token), Boolean.class).getBody();
	}

	@SuppressWarnings("unchecked")
	public Long userConnected(final String token) {
		final LinkedHashMap<String, ?> data = restTemplate.exchange("http://" + authApiPath + "/authenticated",
				HttpMethod.GET, httpHeaders(token), LinkedHashMap.class).getBody();
		final String username = (String) data.get("unique");
		return Long.valueOf(username);
	}

	/**
	 * Check whether the path exist
	 * @param path
	 * @return True when path is referenced
	 */
	public boolean pathExist(final String path) {

		for (final Pattern pattern : paths)
		{
			final Matcher matcher = pattern.matcher(path);
			if(matcher.matches()) {
				return true;
			}
		}
		return false;
	}
}
